import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import setupAxios from './utils/interceptors'
import axios from 'axios'
import VueAxios from 'vue-axios'
import config from './config'
import jQuery from 'jquery'
import 'popper.js'
import 'bootstrap/dist/js/bootstrap.min.js'
global.jQuery = jQuery
global.$ = jQuery

Vue.use(VueAxios, axios)
Vue.config.productionTip = false
Vue.axios.defaults.baseURL = config.baseURL

setupAxios()
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
